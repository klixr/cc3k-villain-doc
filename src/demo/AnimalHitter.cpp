#include "AnimalHitter.hpp"
#include "Animal.hpp"

// 实际上只需要引用一个 Animal.hpp 头文件即可

void Human::hit(Cat * cat) {
    std::cout << "Human: [hit cat] I hit a cat!" << std::endl;
}

void Human::hit(Dog * dog) {
    std::cout << "Human: [hit dog] I hit a dog!" << std::endl;
}

void Human::be_biten(Animal * animal) {
    animal->bite(this);
    std::cout << "Human: Ouch!" << std::endl;
}
