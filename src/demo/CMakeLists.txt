add_executable(demo 
    "Animal.hpp" "AnimalHitter.hpp" "AnimalHitter.cpp" "demo.cpp")

add_executable(rogue-mini
    "Player.hpp" "Floor.hpp" "Floor.cpp" "rogue-mini.cpp")
