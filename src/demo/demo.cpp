#include "Animal.hpp"
#include "AnimalHitter.hpp"

int main() {
    Animal* animals[2] = { new Cat(), new Dog() };
    AnimalHitter* human = new Human();

    for (auto animal : animals) {
        std::cout << "---------\n";
        std::cout << "The human is going to hit an animal.\n";
        animal->be_hit_by(human);
        std::cout << "An animal is going to bite the human.\n";
        human->be_biten(animal);
        std::cout << "---------" << std::endl;
    }

    for (auto p : animals) { delete p; }
    delete human;
}
